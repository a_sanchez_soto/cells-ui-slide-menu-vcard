import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-ui-slide-menu-vcard-styles.js';
import '@cells-components/coronita-icons';
import '@cells-components/cells-icon';
import '@vcard-components/cells-util-behavior-vcard';
/**
This component ...

Example:

```html
<cells-ui-slide-menu-vcard></cells-ui-slide-menu-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
const utilBehavior = CellsBehaviors.cellsUtilBehaviorVcard;
export class CellsUiSlideMenuVcard extends utilBehavior(LitElement) {
  static get is() {
    return 'cells-ui-slide-menu-vcard';
  }

  // Declare properties
  static get properties() {
    return {
      options: {
        type: Array
      },
      stateTransition: {
        type: Boolean
      },
      widthMenu: {
        type: Number
      }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.widthMenu = 320;
    this.options = [];
  }

  toggle() {
    let menuLeft = this.shadowRoot.querySelector('.slide-menu');
    if(menuLeft.classList.contains('hideMenuLeft')){
      menuLeft.classList.remove('hideMenuLeft');
      menuLeft.classList.add('showMenuLeft');
    }else{
      menuLeft.classList.remove('showMenuLeft');
      menuLeft.classList.add('hideMenuLeft');
    }
  }

  hideMenuSlide() {
    let menuLeft = this.shadowRoot.querySelector('.slide-menu');
    menuLeft.classList.remove('showMenuLeft');
    menuLeft.classList.add('hideMenuLeft');
  }

  optionSelected(event) {
    this.toggle();
    let selected = event.path.filter((element) => {
      return element.classList && element.classList.contains('item-opcion-menu'); 
    });
    let option = {} 
    if(selected && selected[0] && selected[0].dataset){
      option = selected[0].dataset.option;
      if(typeof option === 'string') {
        option = JSON.parse(option);
      }
    }
    this.dispatch(this.events.optionMenuSelected, option);
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-ui-slide-menu-vcard-shared-styles').cssText}
    `;
  }

  buildIcon(option) {
   let arr = option.valor.split('#');
   if(arr.length === 1){
    return 'oneclick';
   }else if(arr.length === 2){
    return arr[1].trim();
   }
   return 'oneclick';
  }

  logOut(event) {
    this.dispatch(this.events.logOut, {});
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <div class = "slide-menu hideMenuLeft" >
        
        ${this.options.map(option => html`
          <div class = "item-opcion-menu"  data-option = "${JSON.stringify(option)}"  @click = "${this.optionSelected}" >
            <cells-icon icon="coronita:${this.buildIcon(option)}" class = "option-icon" size = "18" ></cells-icon> ${option.nombre}
          </div>
        `)}
        
        <div class = "item-opcion-exit" @click = "${this.logOut}">
          <cells-icon icon="coronita:on" class = "option-icon opcion-salir" size = "18" ></cells-icon> Cerrar Sesión
        </div>

      </div>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsUiSlideMenuVcard.is, CellsUiSlideMenuVcard);
